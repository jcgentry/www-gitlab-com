---
layout: markdown_page
title: "IAM.1.01 - Logical Access Provisioning Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# IAM.1.01 - Logical Access Provisioning

## Control Statement

Logical access provisioning to information systems requires approval from appropriate personnel.

## Context

The purpose of this control is to ensure there is a process in place to review and authorize new user account requests. Ensuring only people who require access to a system or service receive access helps improve GitLab's overall security posture by limiting the number of accounts with access and reducing the overall likelihood of an account being compromised.

## Scope

This control applies to any system or service where user accounts can be provisioned.

## Ownership

TBD

## Implementation Guidance

For detailed implementation guidance relevant to GitLab team-members, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/IAM.1.01_logical_access_provisioning.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/IAM.1.01_logical_access_provisioning.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/IAM.1.01_logical_access_provisioning.md).

## Framework Mapping

* ISO
  * A.9.2.1
  * A.9.2.2
  * A.9.2.3
  * A.9.4.1
  * A.12.5.1
  * A.18.1.3
* SOC2 CC
  * CC6.1
  * CC6.2
  * CC6.3
  * CC6.6
  * CC6.7
* PCI
  * 7.1.4
  * 8.1.2
